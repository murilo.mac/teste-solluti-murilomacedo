/* Validações de email-->
   1 - Email precisa possuir ao menos um arroba e um ponto
   2 - O arroba não pode estar na primeira posição
   3 - Deve ter pelo menos um caractere e um arroba antes do último ponto
   4 - O email não pode terminar com um ponto
   5 - Não pode ter um ponto logo depois do arroba 
   6 - Não pode ter espaços em branco
   7 - Não pode ter mais de um arroba 
*/

//Validação de email
function ValidarEmail() {
    let email = document.frLogin.email.value;
    if (email.length < 5 || email.indexOf('@') < 1 || email.lastIndexOf('.') < email.indexOf('@') 
        || email.lastIndexOf('.') == email.length - 1) //4 primeiras validações      
    {
        alert("Forneça um email válido!");
        return false;
    }
    else {
        let cont_arroba = 0; //contador de arrobas
        for (let i = 0; i < email.length; i++) {
            let letter = email[i];
            if (letter == '@') {
                cont_arroba++;
            }

            if (email[i] == '@' && email[i + 1] == '.') //Quinta validação, o caractere depois do arroba não pode ser um ponto
            {
                alert("Deve possuir pelo menos um caractere entre o '@' e o ponto!");
                return false;
            }

            if(email[i] == " ") //Sexta Validação, não pode ter espaço em branco
            {
                alert("Um email não pode ter espaços em branco!");
                return false;
            }
        }
        if (cont_arroba > 1) //Sétima validação, não pode ter mais de um arroba
        {
            alert("Um email não pode ter mais de um '@'!");
            return false;
        }
        else {
            return true; //Email válido
        }
    }
}

//Validação de senha
function ValidarSenha() {
    let senha = document.frLogin.senha.value;
    if (senha.length == 0) {
        alert("Preencha o campo 'Senha'"); //Senha não pode estar vazia
        return false;
    }
    else if (senha.length > 6) {
        alert("A senha deve conter no máximo 6 caracteres!");
        return false;
    }
    else {
        return true;
    }
}

//Função passada para avaliar email e senha. Os dois devem ser 'true' para validação total
function Validar() {
    if (ValidarEmail() == true && ValidarSenha() == true) {
        alert("Login feito com sucesso!");
        window.open("https://www.americanas.com.br/");
        return true;        
    }
    else
    { return false; }
}